# Spanish translations for kdeconnect-indicator.po package.
# Copyright (C) 2020 This file is copyright:
# This file is distributed under the same license as the kdeconnect-kde package.
#
# Víctor Rodrigo Córdoba <vrcordoba@gmail.com>, %Y.
# Automatically generated, 2020.
# Eloy Cuadra <ecuadra@eloihr.net>, 2020.
# Víctor Rodrigo Córdoba <vrcordoba@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-indicator\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-12-22 03:10+0100\n"
"PO-Revision-Date: 2020-02-01 17:20+0100\n"
"Last-Translator: Víctor Rodrigo Córdoba <vrcordoba@gmail.com>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Víctor Rodrigo Córdoba"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "vrcordoba@gmail.com"

#: deviceindicator.cpp:50
#, kde-format
msgid "No Battery"
msgstr "Sin batería"

#: deviceindicator.cpp:52
#, kde-format
msgid "Battery: %1% (Charging)"
msgstr "Batería: %1% (Cargando)"

#: deviceindicator.cpp:54
#, kde-format
msgid "Battery: %1%"
msgstr "Batería: %1%"

#: deviceindicator.cpp:87
#, kde-format
msgid "Browse device"
msgstr "Navegar dispositivo"

#: deviceindicator.cpp:95
#, kde-format
msgid "Ring device"
msgstr "Hacer sonar el dispositivo"

#: deviceindicator.cpp:103
#, kde-format
msgid "Send file"
msgstr "Enviar archivo"

#: deviceindicator.cpp:105
#, kde-format
msgid "Select file to send to '%1'"
msgstr "Seleccione archivo a enviar a «%1»"

#: deviceindicator.cpp:119
#, kde-format
msgid "SMS Messages..."
msgstr "Mensajes SMS..."

#: deviceindicator.cpp:126
#, kde-format
msgid "Run command"
msgstr "Ejecutar orden"

#: deviceindicator.cpp:128
#, kde-format
msgid "Add commands"
msgstr "Añadir órdenes"

#: indicatorhelper_mac.cpp:43
#, kde-format
msgid "Launching"
msgstr "Lanzando"

#: indicatorhelper_mac.cpp:78
#, kde-format
msgid "Launching daemon"
msgstr "Lanzando demonio"

#: indicatorhelper_mac.cpp:84 indicatorhelper_mac.cpp:112
#, kde-format
msgid "KDE Connect"
msgstr "KDE Connect"

#: indicatorhelper_mac.cpp:85
#, kde-format
msgid "Cannot find kdeconnectd"
msgstr "No se pudo encontrar kdeconnectd"

#: indicatorhelper_mac.cpp:93
#, kde-format
msgid "Waiting D-Bus"
msgstr "Esperando a D-Bus"

#: indicatorhelper_mac.cpp:113
#, kde-format
msgid ""
"Cannot connect to DBus\n"
"KDE Connect will quit"
msgstr ""
"No se pudo conectar a DBus\n"
"KDE Connect se cerrará"

#: indicatorhelper_mac.cpp:124
#, kde-format
msgid "Loading modules"
msgstr "Cargando módulos"

#: main.cpp:49
#, kde-format
msgid "KDE Connect Indicator"
msgstr "Indicador de KDE Connect"

#: main.cpp:51
#, kde-format
msgid "KDE Connect Indicator tool"
msgstr "Herramienta del indicador de KDE Connect"

#: main.cpp:53
#, kde-format
msgid "(C) 2016 Aleix Pol Gonzalez"
msgstr "© 2016 Aleix Pol Gonzalez"

#: main.cpp:75
#, kde-format
msgid "Configure..."
msgstr "Configurar..."

#: main.cpp:91
#, kde-format
msgid "Pairing requests"
msgstr "Peticiones de emparejamiento"

#: main.cpp:96
#, kde-format
msgid "Pair"
msgstr "Emparejar"

#: main.cpp:97
#, kde-format
msgid "Reject"
msgstr "Rechazar"

#: main.cpp:103
#, kde-format
msgid "Quit"
msgstr "Quitar"

#: main.cpp:131 main.cpp:145
#, kde-format
msgid "%1 device connected"
msgid_plural "%1 devices connected"
msgstr[0] "%1 dispositivo conectado"
msgstr[1] "%1 dispositivos conectados"
